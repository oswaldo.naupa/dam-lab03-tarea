import React, {Component} from 'react';
import {StyleSheet, TouchableOpacity,TextInput, Text, View, Image} from 'react-native';
import Validator from './app/components/ageValidator';
import Lista from './app/components/myList';
export default class App extends Component {
  constructor (props) {
    super(props);
    this.state ={
      textValue: '',
      count: 0,
    };
  }
  changeTextInput = text => {
    let newText = '';
    let numbers = '0123456789';

    for (var i=0; i < text.length; i++) {
      if(numbers.indexOf(text[i]) > -1 ) {
          newText = newText + text[i];
      }
      else {
          // your call back function
          alert("Solo se permiten numeros");
      }
  }
  this.setState({ textValue: newText });
  };

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.text}>Validacion de Edad:</Text>
        <Text> Ingrese su edad:</Text>
        <Validator textValueAge={this.state.textValue} changeBody={this.changeTextInput}/>  
        <Text style={styles.text}>Mejores Juegos Play Station:</Text>
        <Lista/>
      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    padding: 10,
  },
  text: {
    alignItems: 'center',
    fontWeight: 'bold',
    fontSize: 25,
  },
  image: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    top: 10,
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10,
  },
  countContainer: {
    alignItems: 'center',
    padding: 10,
  },
  countText: {
    color: '#FF00FF',
  },
})

