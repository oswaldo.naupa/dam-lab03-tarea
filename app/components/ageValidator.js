import React from 'react';
import {View, Text, TextInput,StyleSheet} from 'react-native';

const Validator = props => (
   <View>
    <TextInput 
    style = {{height: 40, borderColor: 'gray', borderWidth: 1}}
    onChangeText= {text=> props.changeBody(text)}
    value= {props.textValueAge}
    />
    {
      props.textValueAge == "" ? <Text style={styles.textVacio}>Campo vacio</Text>
      : 
      props.textValueAge >= 18 ? <Text style={styles.textMayor}>Eres Mayor de Edad</Text>
      : <Text style={styles.TextMenor}>Eres Menor de Edad</Text>
    }
  </View> 
);
const styles = StyleSheet.create({
  textMayor: {
    color: 'blue'
  },
  TextMenor: {
    color: 'green'
  },
  textVacio: {
    color: 'red'
  },
});
export default Validator