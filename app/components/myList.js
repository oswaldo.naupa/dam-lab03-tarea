import React from 'react';
import {View, Text,StyleSheet, SafeAreaView, FlatList, Image} from 'react-native';

const DATA = [
    {
      id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
      title: 'THE LAST OF US',
      subtitle: 'The Last of Us es un videojuego de acción-aventura y supervivencia de terror',
      url:require('./img/imagen1.jpg'),
    },
    {
      id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
      title: 'UNCHARTED 2',
      subtitle: 'Es un juego de acción y aventura de 2009 desarrollado por Naughty Dog y publicado por Sony Computer Entertainment',
      url:require('./img/imagen2.jpg'),
    },
    {
      id: '58694a0f-3da1-471f-bd96-145571e29d72',
      title: 'METAL GEAR SOLID 4',
      subtitle: 'Es un juego de acción y aventura de 2008desarrollado por Kojima Productions y publicado por Konami exclusivamente para la consola PlayStation 3',
      url:require('./img/imagen3.jpg'),
    },
    {
        id: '58694a0f-3da1-471f-bd96-145571e29d73',
        title: 'GTA V',
        subtitle: 'Grand Theft Auto V es un videojuego de acción-aventura de mundo abierto desarrollado por el estudio Rockstar North y distribuido por Rockstar Games.',
        url:require('./img/imagen4.jpg'),
      },
      {
        id: '58694a0f-3da1-471f-bd96-145571e29d74',
        title: 'RED DEAD REDEMPTION',
        subtitle: 'Red Dead Redemption es un videojuego no lineal de acción-aventura western desarrollado por Rockstar San Diego',
        url:require('./img/imagen5.jpg'),
      },
      {
        id: '58694a0f-3da1-471f-bd96-145571e29d75',
        title: 'BATMAN: ARKHAM CITY',
        subtitle: 'Batman: Arkham City es un videojuego de acción y aventura lanzado en el año 2011 desarrollado por Rocksteady Studios',
        url:require('./img/imagen6.jpg'),
      },
    
  ];
const Lista = () => (
      <FlatList
        data={DATA}
        renderItem={({ item }) => 
            <View style={{flex:1, flexDirection: 'row'}}  >
                <Image  source={item.url} style={styles.imageView}/>
                <View style={styles.textView}>
                <Text style={{textAlign:'justify'}} style={styles.textTitle}>{item.title}</Text>
                <Text style={{textAlign:'justify'}}>{item.subtitle}</Text>
                </View>
            </View>}
        keyExtractor={item => item.id}
      />
    
);
const styles = StyleSheet.create({
    imageView: {
 
        width: '30%',
        height: 110 ,
        margin: 7,
        borderRadius : 30
     
    },
    textView: {
        width:'70%', 
        textAlignVertical:'center',
        padding:20,
        color: '#000',
        textAlign:'justify'
     
    },
    textTitle:{
        fontWeight: 'bold',
    },
  });
export default Lista